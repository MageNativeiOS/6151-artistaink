//
//  UserErrorViewModel.swift
//  MageNative Shopify App
//
//  Created by cedcoss on 03/08/18.
//  Copyright © 2018 MageNative. All rights reserved.
//

import Foundation

final class UserErrorViewModel:ViewModel{
      typealias ModelType = Storefront.UserError
    let model:  ModelType?
    let errorMessage:String
    let errorFields:[String]?
    
    required init(from model:ModelType) {
        
        self.model = model
        self.errorMessage = model.message
        self.errorFields = model.field
    }
}

extension Storefront.UserError: ViewModeling {
    typealias ViewModelType = UserErrorViewModel
}
