/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
protocol wishListDelegate {
    func addToWishListProduct(_ cell: ProductCollectionViewCell, didAddToWishList sender: Any)
}

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var addToWishList: UIButton!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productPrice: UILabel!
    var delegate:wishListDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
         addToWishList.addTarget(self, action: #selector(self.addItemToWishList(_:)), for: .touchUpInside)
        productPrice.numberOfLines = 0
    }
    
    func setupView(_ model:ProductViewModel){
        productName?.text = model.title
        productPrice.attributedText =  self.calCulatePrice(model)
        productImage.setImageFrom(model.images.items.first?.url)
        if WishlistManager.shared.isProductinWishlist(product: model){
            addToWishList.setImage(#imageLiteral(resourceName: "wishfilled"), for: .normal)
        }else {
            addToWishList.setImage(#imageLiteral(resourceName: "wishempty"), for: .normal)
        }
    }
    
    
   @objc func addItemToWishList(_ sender:UIButton){
          self.delegate?.addToWishListProduct(self, didAddToWishList: sender)
    }
    
    
    func calCulatePrice(_ model:ProductViewModel) -> NSMutableAttributedString?{
        let price = NSMutableAttributedString()
        let attr1 = [NSAttributedString.Key.foregroundColor:UIColor.red,NSAttributedString.Key.font:UIFont(name: "Helvetica", size: mageFont.regularsize)]
      
        if model.compareAtPrice != "false"{
            let attribute1 = NSAttributedString(string:  model.price, attributes: attr1  as [NSAttributedString.Key : Any])
            price.append(attribute1)
          
            let attr = [NSAttributedString.Key.strikethroughStyle:1,NSAttributedString.Key.font:UIFont(name: "Helvetica", size: mageFont.regularsize) ?? ""] as [NSAttributedString.Key : Any]
                price.append(NSAttributedString(string: " "))
                let attribute = NSAttributedString(string:  model.compareAtPrice, attributes: attr)
                price.append(attribute)

            return price

        }else {
            let attribute = NSAttributedString(string:  model.price)
            price.append(attribute)
            return price
        }
       
    }
}
