/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation

class CartProduct:Equatable,Serializable {
    
    private struct CartKey {
        static let product  = "product"
        static let quantity = "quantity"
        static let variant  = "variant"
    }
    
    let productModel:ProductViewModel
    let variant:VariantViewModel
    var qty=1
    
    required init(product: ProductViewModel, variant: VariantViewModel, quantity: Int = 1) {
        self.productModel  = product
        self.variant  = variant
        self.qty = quantity
    }
    
    
    // ----------------------------------
    //  MARK: - Serializable -
    //
    static func deserialize(from representation: SerializedRepresentation) -> Self? {
        guard let product = ProductViewModel.deserialize(from: representation[CartKey.product] as! SerializedRepresentation) else {
            return nil
        }
        
        guard let variant = VariantViewModel.deserialize(from: representation[CartKey.variant] as! SerializedRepresentation) else {
            return nil
        }
        
        guard let quantity = representation[CartKey.quantity] as? Int else {
            return nil
        }
        
        return self.init(
            product:  product,
            variant:  variant,
            quantity: quantity
        )
    }
    
    func serialize() -> SerializedRepresentation {
       
            return [
                CartKey.quantity : self.qty,
                CartKey.product  : self.productModel.serialize(),
                CartKey.variant  : self.variant.serialize(),
            ]
        
        
    }
    
    
}

extension CartProduct{
    static func == (lhs: CartProduct, rhs: CartProduct) -> Bool {
        return (lhs.productModel.id == rhs.productModel.id) && (lhs.variant.id == rhs.variant.id)
    }
    
}
