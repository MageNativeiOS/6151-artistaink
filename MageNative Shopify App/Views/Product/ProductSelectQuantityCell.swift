//
//  ProductSelectQuantityCell.swift
//  MageNative Shopify App
//
//  Created by Manohar Singh Rawat on 25/01/20.
//  Copyright © 2020 MageNative. All rights reserved.
//

import UIKit

class ProductSelectQuantityCell: UITableViewCell {

    @IBOutlet weak var decrementQtyButton: UIButton!
    
    @IBOutlet weak var incrementQtyButton: UIButton!
    
    @IBOutlet weak var qtyTextField: SkyFloatingLabelTextField!
    
    var parent = ProductViewController()
    
    var qty = 1;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        qtyTextField.text = "1";
        qtyTextField.textAlignment = .center
        incrementQtyButton.addTarget(self, action: #selector(incrementQty(sender:)), for: .touchUpInside);
        decrementQtyButton.addTarget(self, action: #selector(decrementQty(sender:)), for: .touchUpInside)
        // Initialization code
    }

    @objc func incrementQty(sender:UIButton){
        guard let qtyText = self.qtyTextField.text else {return}
        guard  var qty = Int(qtyText) else {return}
        qty += 1
        qtyTextField.text = "\(qty)"
        parent.qty = "\(qty)"
        //delegate?.updateCartQuantity(sender: self, quantity:qty, model: self.model)
    }
    
    @objc func decrementQty(sender:UIButton){
        guard let qtyText = self.qtyTextField.text else {return}
        guard  var qty = Int(qtyText) else {return}
        qty -= 1
        if qty <= 0  {return}
        qtyTextField.text = "\(qty)"
        parent.qty = "\(qty)"
        //delegate?.updateCartQuantity(sender: self, quantity:qty, model: self.model)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
