/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

protocol ProductImageClicked:class {
    func productImageClicked(indexPath:IndexPath,sender:ProductHeaderViewCell)
}

class ProductHeaderViewCell: UITableViewCell {
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var ImageCollectionView: UICollectionView!
    weak var delegate:ProductImageClicked?
    
    var images : [ImageViewModel]? {
        didSet {
            guard let images = images else {return}
            pageControl.numberOfPages = images.count
            self.ImageCollectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        ImageCollectionView.delegate = self
        ImageCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension ProductHeaderViewCell:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell       = collectionView.dequeueReusableCell(withReuseIdentifier: "productImage", for: indexPath)
        if let contentView =  cell.viewWithTag(852) as? UIImageView {
            contentView.setImageFrom(images?[indexPath.row].url)
        }
        return cell
    }
    
    @objc func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(self.ImageCollectionView.contentOffset.x) / Int(self.ImageCollectionView.frame.size.width);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.productImageClicked(indexPath: indexPath, sender: self)
    }
    
}


extension ProductHeaderViewCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
}
