/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
protocol SelectVariantCellDelegate:class {
    func showSelectVariantController(_ cell: ProductSelectVariantCell)
}

class ProductSelectVariantCell: UITableViewCell {

    @IBOutlet weak var variantStack: UIStackView!
     weak var delegate: SelectVariantCellDelegate?
    var model: VariantViewModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.cellClicked))
        gesture.delegate = self
        self.addGestureRecognizer(gesture)
    }
    
    func configure(from model:VariantViewModel,for control:ProductViewController){
        self.model = model
 
        guard self.variantStack.arrangedSubviews.count == 0 else {return}
        model.selectedOptions.forEach({
            let label = UILabel()
            label.text = $0.name + "\n" + $0.value
            label.textColor = UIColor.blue
            label.font = UIFont.systemFont(ofSize: 12)
            label.numberOfLines = 0
            label.textAlignment = .center
            self.variantStack.addArrangedSubview(label)
        })
        guard  let cell =  control.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProductHeaderViewCell else {return}
      guard let index =  cell.images?.index(where: {
            $0.url == model.image
      }) else{return}
        let indexPath = IndexPath(row: index, section: 0)
        cell.ImageCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func cellClicked(gesture:UITapGestureRecognizer){
        self.delegate?.showSelectVariantController(self)
    }

}
