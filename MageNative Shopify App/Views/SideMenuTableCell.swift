/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class SideMenuTableCell : UITableViewCell {
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet private weak var detailsLabel: UILabel!
    @IBOutlet private weak var customTitleLabel: UILabel!
    @IBOutlet weak var img: UIImageView!
     var additionButtonActionBlock : ((SideMenuTableCell) -> Void)?;

    override func awakeFromNib() {
        selectedBackgroundView? = UIView()
        selectedBackgroundView?.backgroundColor = .clear
    }
    
   
    func setup(from menu :MenuObject,level:Int) {
        customTitleLabel.text = menu.name
        self.img.image=UIImage(named: "categorywhite")
        detailsLabel.text = ""
        self.backgroundColor=UIColor.clear
        self.img.isHidden=true
        let left = 11.0 + 20.0 * CGFloat(level)
        self.customTitleLabel.frame.origin.x = left+30
        self.img.frame.origin.x = left
        self.detailsLabel.frame.origin.x = left+30
    }
    
   
    
}
