/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UserNotifications
extension AppDelegate:UNUserNotificationCenterDelegate {
    func registerForPushNotification(application:UIApplication){
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        //        UNUserNotificationCenter.current().delegate = self
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        let udid=UIDevice.current.identifierForVendor!.uuidString
        if deviceTokenString != ""
        {
            
            let url=(AppSetUp.baseUrl+"index.php/shopifymobile/shopifyapi/setdevices?mid="+Client.merchantID+"&device_id="+deviceTokenString+"&email=&type=ios&unique_id="+udid).getURL()
            
            var urlreq=URLRequest(url: url!)
            urlreq.httpMethod="GET"
            urlreq.setValue("application/json", forHTTPHeaderField: "Content-Type")
            AF.request(urlreq).responseData(completionHandler: {
                response in
                switch  response.result {
                case .success:
                    print("Success")
                case .failure:
                    print("Failure")
                }
                
            })
            
        }
        // Persist it in your backend in case it's new
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let state = UIApplication.shared.applicationState
        let test=userInfo["aps"] as! Dictionary<String,Any>
        let main=test["alert"] as! Dictionary<String,Any>
        if state == .active {
            
            
            let alert=UIAlertController(title: main["title"] as! String, message: main["body"] as! String, preferredStyle: .alert)
            let action=UIAlertAction(title: "Ok".localized, style: .default, handler: { (action: UIAlertAction!) in
                self.openPages(main: main)
                
            })
            let action_cancel=UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { (action: UIAlertAction!) in
                return
            })
            alert.addAction(action)
            alert.addAction(action_cancel)
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            return
        }
        else if state == .background{
            self.openPages(main: main)
        }
        else if state == .inactive{
            self.openPages(main: main)
        }
        else{
            self.openPages(main: main)
        }
    }
    
    func openPages(main:Dictionary<String,Any>){
        print(main)
        var tempMain=main
        tempMain.removeValue(forKey: "expired_on")
        UserDefaults.standard.set(tempMain, forKey: "MageNotificationData")
        
        let notType=main["link_type"] as! String
        let notification_id=main["notification_id"] as! String
        if UserDefaults.standard.value(forKey: "NotificationIDS") != nil
        {
            let temp=UserDefaults.standard.value(forKey: "NotificationIDS") as! String
            let data=","+notification_id
            UserDefaults.standard.set(temp+data, forKey: "NotificationIDS")
        }
        else
        {
            UserDefaults.standard.set(notification_id, forKey: "NotificationIDS")
        }
        if(notType == "collection"){
            let data=main["link_id"] as! String
            let str="gid://shopify/Collection/"+data
            let str1 = (str).data(using: String.Encoding.utf8)
            let base64 = str1!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            print(base64)
            let collectionId=GraphQL.ID(rawValue: base64)
            
            UserDefaults.standard.set(["collection":collectionId.description], forKey: "NotificationData")
            
            //let drawer=cedMageSideDrawer.init(drawerDirection: .Left, drawerWidth: 275)
            let drawer=UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            
            self.window?.rootViewController=drawer
            self.window?.makeKeyAndVisible()
            
        }else if (notType == "product"){
            let data=main["link_id"] as! String
            let str="gid://shopify/Product/"+data
            let str1 = (str).data(using: String.Encoding.utf8)
            let base64 = str1!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            print(base64)
            let productId=GraphQL.ID(rawValue: base64)
            
            UserDefaults.standard.set(["product":productId.description], forKey: "NotificationData")
            
            let drawer=UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            
            self.window?.rootViewController=drawer
            
            self.window?.makeKeyAndVisible()
            
        }else if (notType == "web_address"){
            let data=main["link_id"] as! String
            //let url=URL(string: data!)
            
            UserDefaults.standard.set(["web":data], forKey: "NotificationData")
            
            let drawer=UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            
            self.window?.rootViewController=drawer
            self.window?.makeKeyAndVisible()
            
        }
        return
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if UIApplication.shared.applicationState == .background ||    UIApplication.shared.applicationState == .inactive {
            if CartManager.shared.cartCount > 0 {
                completionHandler([.alert, .sound])
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "magenative_cart_notification_center" {
            let cartSelect = ["link_type":"cart"]
            self.openPages(main: cartSelect)
            
        }
        
        completionHandler()
        
    }
}
