/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation

final class Client {
    static let shopUrl    = "artistaink.myshopify.com"
    static let merchantID = "6151"
    static let apiKey     = "6f9fc6767d5f1151f39165632cf0e9a7"
    private let defaults = UserDefaults.standard
    static let loginUrl = "https://\(Client.shopUrl)/account/login"
    
    static let shared = Client()
    
    private let client: Graph.Client = Graph.Client(shopDomain: Client.shopUrl, apiKey: Client.apiKey)
    
    // --------------------------------
    //  MARK: - Init
    //
    private init() {
        self.client.cachePolicy = .cacheFirst(expireIn: 3600)
    }
    
    // --------------------------------
    //  MARK: - SaveToken
    //
    func saveCustomerToken(token:String,expiry:Date,email:String,password:String){
        let tokenInfo = ["token":token,"expiry":expiry,"email":email,"mageSPVal":password] as [String:Any]
        defaults.set(tokenInfo, forKey: "mageInfo")
        defaults.set(true, forKey: "mageShopLogin")
    }
    
    //----------------------------------
    //  MARK: - CheckForLogin
    //
    func isAppLogin()->Bool{
       return defaults.bool(forKey: "mageShopLogin")
    }
    
    //----------------------------------
    // MARK: - return the token
    //
    
    func getToken()->Any? {
        return defaults.value(forKey: "mageInfo")
    }
    
    
    //----------------------------------
    // MARK: - save the Currency
    //
    func saveCurrencyCode(currency:String){
        defaults.set(currency, forKey: "mageCurrencyCode")
    }
    
    
    //----------------------------------
    // MARK: - Get the Currency
    //
    func getCurrencyCode()->String?{
        return defaults.string(forKey:  "mageCurrencyCode")
    }
    
    
    //----------------------------------
    // MARK: - doLogout
    //
    
    func doLogOut()->Bool{
        defaults.removeObject(forKey: "mageInfo")
        defaults.set(false, forKey: "mageShopLogin")
        return true
    }
    
    // ----------------------------------
    //  MARK: - ShopName -
    //
    @discardableResult
    func fetchShop(completion: @escaping (ShopViewModel?) -> Void) -> Task {
        
        let query = ClientQuery.queryForShop()
        let task  = self.client.queryGraphWith(query) { (query, error) in
            error.errorPrint()
            
            if let query = query {
                completion(query.shop.viewModel)
            } else {
                print("Failed to fetch shop name: \(String(describing: error))")
                completion(nil)
            }
        }
        
        task.resume()
        return task
    }
    
    
    // ----------------------------------
    //  MARK: - ShopAllProducts -
    //
    @discardableResult
    func fetchShopAllProducts(limit: Int = 25, after cursor: String? = nil,completion: @escaping (PageableArray<ProductListViewModel>?) -> Void) -> Task {
        
        let query = ClientQuery.queryForAllShopProducts(limit: limit,after:cursor)
        let task  = self.client.queryGraphWith(query) { (query, error) in
            error.errorPrint()
            if let query = query {
                let products = PageableArray(
                    with:     query.shop.products.edges,
                    pageInfo: query.shop.products.pageInfo
                )
                completion(products)
            } else {
                print("Failed to fetch shop name: \(String(describing: error))")
                completion(nil)
            }
        }
        
        task.resume()
        return task
    }
    
    
    
    // ----------------------------------
    //  MARK: - Collections -
    //
    @discardableResult
    func fetchCollections(limit: Int = 25, after cursor: String? = nil, productLimit: Int = 25, productCursor: String? = nil,maxImageWidth:Int32,maxImageHeight:Int32, completion: @escaping (PageableArray<CollectionViewModel>?,Graph.QueryError?) -> Void) -> Task {
        
        let query = ClientQuery.queryForCollections(limit: limit, after: cursor, productLimit: productLimit, productCursor: productCursor, maxHeight: maxImageHeight, maxWidth: maxImageWidth)
        let task  = self.client.queryGraphWith(query) { (query, error) in
            error.errorPrint()
            
            if let query = query {
                let collections = PageableArray(
                    with:     query.shop.collections.edges,
                    pageInfo: query.shop.collections.pageInfo
                )
                completion(collections,error)
            } else {
                print("Failed to load collections: \(String(describing: error))")
                completion(nil,error)
            }
        }
        
        task.resume()
        return task
    }
    
    
    // ----------------------------------
    //  MARK: - Products -
    //
    @discardableResult
    func fetchProducts(in collection: CollectionViewModel? = nil ,coll:collection? = nil,sortKey:Storefront.ProductCollectionSortKeys? = nil,reverse:Bool? = nil, limit: Int = 15, after cursor: String? = nil, completion: @escaping (PageableArray<ProductListViewModel>?,URL?,Graph.QueryError?) -> Void) -> Task {
        
        let query = ClientQuery.queryForProducts(in: collection, coll: coll,with: sortKey,reverse: reverse, limit: limit, after: cursor)
        let task  = self.client.queryGraphWith(query) { (query, error) in
            error.errorPrint()
            
            if let query = query,
                let collection = query.node as? Storefront.Collection {
                let products = PageableArray(
                    with:     collection.products.edges,
                    pageInfo: collection.products.pageInfo
                )
                completion(products,collection.image?.transformedSrc,error)
                
            } else {
                print("Failed to load products in collection (\(String(describing: collection?.model?.node.id.rawValue))): \(String(describing: error))")
                completion(nil,nil,error)
            }
        }
        
        task.resume()
        return task
    }
    
    //-----------------------------------
    // MARK:- Apply CouponCode
    //
    @discardableResult
    func applyCouponCode(_ discountCode: String, to checkoutID: String, completion: @escaping (CheckoutViewModel?,Graph.QueryError?) -> Void)->Task{
        let mutation = ClientQuery.mutationForApplyCopounCode(discountCode, to: checkoutID)
        let task     = self.client.mutateGraphWith(mutation) { response, error in
            error.errorPrint()
            if let query =  response?.checkoutDiscountCodeApply {
                completion(query.checkout.viewModel,error)
                
            }else {
                completion(nil,error)
            }
           
            
        }
        task.resume()
        return task
    }
   
    
    
    // ----------------------------------
    //  MARK: - Checkout -
    //
    @discardableResult
    func createCheckout(with cartItems: [CartProduct], completion: @escaping (CheckoutViewModel?,[UserErrorViewModel]?) -> Void) -> Task {
        let mutation = ClientQuery.mutationForCreateCheckout(with: cartItems)
        let task     = self.client.mutateGraphWith(mutation) { response, error in
            error.errorPrint()
        completion(response?.checkoutCreate?.checkout?.viewModel,response?.checkoutCreate?.userErrors.viewModels)
        }
        
        
        task.resume()
        return task
    }
    
    
    // ----------------------------------
    //  MARK: - SIGNUP -
    //
    @discardableResult
    func createCustomer(with fname:String,lname:String,email:String,password:String,newsletter:Bool,completion:@escaping(CustomerViewModel?,[UserErrorViewModel]?,Graph.QueryError?)->Void)->Task{
        let mutation = ClientQuery.queryForSignUp(email: email, password: password, firstName: fname, lastName: lname, acceptsMarketing: newsletter)
        let task     = self.client.mutateGraphWith(mutation) { response, error in
                error.errorPrint()
            if let response =  response?.customerCreate {
                
            completion(response.customer?.viewModel,response.userErrors.viewModels,error)
            }else {
            
                completion(nil,nil,error)
            }
        }


        task.resume()
        return task
    }
    
    
    // ----------------------------------
    //  MARK: - CustomerAccessToken -
    //
    @discardableResult
    func customerAccessToken(email:String,password:String,completion:@escaping(CustomerAccessTokenModel?,[UserErrorViewModel]?,Graph.QueryError?)->Void)->Task{
        let mutation=ClientQuery.queryForCustomerToken(email: email, password: password)
        let task = self.client.mutateGraphWith(mutation){response,error in
            error.errorPrint()
            if let checkout = response?.customerAccessTokenCreate {
                completion(checkout.customerAccessToken?.viewModel,checkout.userErrors.viewModels,error)
            }else {
                completion(nil,nil,error)
            }
                
            
            
        }
        task.resume()
        return task
    }
    
    // ----------------------------------
    //  MARK: - ForgetPassword -
    //
    @discardableResult
    func forgetPassword(with email:String,completion:@escaping([UserErrorViewModel]?,Graph.QueryError?)->Void)->Task{
        let mutation = ClientQuery.mutationForForgetPassword(with: email)
        let task = self.client.mutateGraphWith(mutation){response,error in
            error.errorPrint()
            completion(response?.customerRecover?.userErrors.viewModels,error)
            
        }
        task.resume()
        return task
    }
    
    // ----------------------------------
    //  MARK: - MyOrders -
    //
    @discardableResult
    func fetchCustomerOrders(limit: Int = 15, after cursor: String? = nil, completion: @escaping (PageableArray<OrderViewModel>?,Graph.QueryError?) -> Void) -> Task {
        let tokenArray = getToken() as! [String:Any]
        let accessToken = tokenArray["token"] as! String
        let query = ClientQuery.queryForOrders(of: accessToken, limit: limit, after: cursor)
        let task = self.client.queryGraphWith(query, completionHandler: {
            query,error    in
            error.errorPrint()
            
            if let query = query, let customer = query.customer  {
                let orders = PageableArray(
                    with:     customer.orders.edges,
                    pageInfo: customer.orders.pageInfo
                )
                completion(orders,error)
            } else {
                print("Failed to load collections: \(String(describing: error))")
                completion(nil,error)
            }
        })
        task.resume()
        return task
    }
    
    
    // ----------------------------------
    //  MARK: - MyAddresses -
    //
    @discardableResult
    func fetchCustomerAddresses(limit: Int = 15, after cursor: String? = nil, completion: @escaping (PageableArray<AddressesViewModel>?,Graph.QueryError?) -> Void) -> Task {
        let tokenArray = getToken() as! [String:Any]
        let accessToken = tokenArray["token"] as! String
        let query = ClientQuery.queryForAddress(accessToken: accessToken, limit: limit)
        self.client.cachePolicy = .networkOnly
        let task = self.client.queryGraphWith(query, completionHandler: {
            query,error    in
            error.errorPrint()
            
            if let query = query, let customer = query.customer  {
                let address = PageableArray(
                    with:     customer.addresses.edges,
                    pageInfo: customer.addresses.pageInfo
                )
                completion(address,error)
            } else {
                print("Failed to load collections: \(String(describing: error))")
                completion(nil,error)
            }
        })
        task.resume()
        return task
    }
    
    // ----------------------------------
    //MARK : - FetchCustomer Details
    //
    @discardableResult
    func fetchCustomerDetails(completeion: @escaping(CustomerViewModel?,Graph.QueryError?)->Void)->Task{
        let tokenArray = getToken() as! [String:Any]
        let token = tokenArray["token"] as! String
        
        let query = ClientQuery.queryForCustomer(accessToken: token)
        let task = self.client.queryGraphWith(query, completionHandler: {
            response,error in
            error.errorPrint()
            if let response = response {
               completeion(response.customer?.viewModel,error)
            }
        
        })
        task.resume()
        return task
    }
    
    //----------------------------------
    //Mark : - Search Products
    //
    @discardableResult
    func searchProductsForQuery(for search:String,limit: Int = 15, after cursor: String? = nil, completion: @escaping (PageableArray<ProductListViewModel>?,Graph.QueryError?) -> Void) -> Task{
        let query = ClientQuery.queryForSearchProducts(for: search, limit: limit, after: cursor)
        let task  = self.client.queryGraphWith(query) { (query, error) in
            error.errorPrint()
           
            if let query = query,let shop = query.shop as? Storefront.Shop  {
               
                let products = PageableArray(
                    with:     shop.products.edges,
                    pageInfo: shop.products.pageInfo
                )
                //print("testing-"+products.items[0].title)
                completion(products,error)
            }else {
                completion(nil,error)
            }
            
        }
        task.resume()
        return task
    }
    
    
    //----------------------------------
    //Mark : - Single product fetch
    //
    
    @discardableResult
    func fetchSingleProduct(of id:String,completion:@escaping(ProductViewModel?,Graph.QueryError?)->Void)->Task{
        let query = ClientQuery.queryForSingleProduct(of:GraphQL.ID(rawValue: id))
        let task  = self.client.queryGraphWith(query) { (query, error) in
            error.errorPrint()
            
            if let query = query,let node = query.node as? Storefront.Product {
                do {
                let productList = try Storefront.ProductEdge.init(fields: node.fields)
                    print(productList,error)
                }catch let error{
                    print(error.localizedDescription)
                }
                completion(node.viewModel,error)
             
            }else {
                completion(nil,error)
            }
            
        }
        task.resume()
        return task
    }
    
    
    //----------------------------------
    //Mark : - Add Address
    //
    @discardableResult
    func customerAddAddress(address:[String:String],completeion: @escaping(AddressViewModel?,[UserErrorViewModel]?)->Void)->Task{
        let tokenArray = getToken() as! [String:Any]
        let token = tokenArray["token"] as! String
        let mutation = ClientQuery.customerAddAddressQuery(accessToken: token, address: address)
        let task  = self.client.mutateGraphWith(mutation){ response, error in
            error.errorPrint()
            if let response = response {
                completeion(
                response.customerAddressCreate?.customerAddress?.viewModel,
                response.customerAddressCreate?.userErrors.viewModels)
            }
            
        }
        task.resume()
       
        return task
    }
    
    
       //----------------------------------
        //Mark : - Update Address
        //
        @discardableResult
        func customerUpdateAddress(address:[String:String],addressId:GraphQL.ID,completeion: @escaping(AddressViewModel?,[UserErrorViewModel]?)->Void)->Task{
            let tokenArray = getToken() as! [String:Any]
            let token = tokenArray["token"] as! String
            let mutation = ClientQuery.customerUpdateAddressQuery(accessToken: token, addressId: addressId, address: address)
            let task  = self.client.mutateGraphWith(mutation){ response, error in
                error.errorPrint()
                if let response = response {
                    completeion(
                        response.customerAddressUpdate?.customerAddress?.viewModel,
                        response.customerAddressUpdate?.userErrors.viewModels)
                }
            }
            task.resume()
            return task
        }
        
        
        //----------------------------------
        //Mark : - Delete Address
        //
        @discardableResult
        func customerDeleteAddress(address:AddressesViewModel,completeion: @escaping(String?,[UserErrorViewModel]?,Graph.QueryError?)->Void)->Task{
       
            let tokenArray = getToken() as! [String:Any]
            let token = tokenArray["token"] as! String
            let mutation = ClientQuery.customerDeleteAddress(addressId: address.id, with: token)
            let task  = self.client.mutateGraphWith(mutation){ response, error in
                error.errorPrint()
                if let response = response {
                    completeion(
                        response.customerAddressDelete?.deletedCustomerAddressId,
                        response.customerAddressDelete?.userErrors.viewModels,error)
                }else {
                    completeion(nil,nil,error)
                }
            }
            task.resume()
            
            return task
        }
    
    
    // ----------------------------------
    //  MARK: - UpdateUser -
    //
    @discardableResult
    func updateCustomer(with fname:String,lname:String,email:String,password:String,completion:@escaping(CustomerViewModel?,[UserErrorViewModel]?,Graph.QueryError?)->Void)->Task{
        let tokenArray = getToken() as! [String:Any]
        let token = tokenArray["token"] as! String
        let mutation = ClientQuery.customerUpdateDetails(accessToken: token, email: email, password: password, firstName: fname, lastName: lname)
        
        let task     = self.client.mutateGraphWith(mutation) { response, error in
            error.errorPrint()
            completion(response?.customerUpdate?.customer?.viewModel,response?.customerUpdate?.userErrors.viewModels,error)
        }
        
        
        task.resume()
        return task
    }
    
    
    @discardableResult
    func updateCheckout(_ id: String, updatingPartialShippingAddress address: PayPostalAddress, completion: @escaping (CheckoutViewModel?) -> Void) -> Task {
        let mutation = ClientQuery.mutationForUpdateCheckout(id, updatingPartialShippingAddress: address)
        let task     = self.client.mutateGraphWith(mutation) { response, error in
            error.errorPrint()
            
            if let checkout = response?.checkoutShippingAddressUpdate?.checkout,
                let _ = checkout.shippingAddress {
                completion(checkout.viewModel)
            } else {
                completion(nil)
            }
        }
        
        task.resume()
        return task
    }
    
    @discardableResult
    func updateCheckout(_ id: String, updatingCompleteShippingAddress address: PayAddress, completion: @escaping (CheckoutViewModel?) -> Void) -> Task {
        let mutation = ClientQuery.mutationForUpdateCheckout(id, updatingCompleteShippingAddress: address)
        let task     = self.client.mutateGraphWith(mutation) { response, error in
            error.errorPrint()
            
            if let checkout = response?.checkoutShippingAddressUpdate?.checkout,
                let _ = checkout.shippingAddress {
                completion(checkout.viewModel)
            } else {
                completion(nil)
            }
        }
        
        task.resume()
        return task
    }
    
    @discardableResult
    func updateCheckout(_ id: String, updatingShippingRate shippingRate: PayShippingRate, completion: @escaping (CheckoutViewModel?) -> Void) -> Task {
        let mutation = ClientQuery.mutationForUpdateCheckout(id, updatingShippingRate: shippingRate)
        let task     = self.client.mutateGraphWith(mutation) { response, error in
            error.errorPrint()
            
            if let checkout = response?.checkoutShippingLineUpdate?.checkout,
                let _ = checkout.shippingLine {
                completion(checkout.viewModel)
            } else {
                completion(nil)
            }
        }
        
        task.resume()
        return task
    }
    
    @discardableResult
    func updateCheckout(_ id: String, updatingEmail email: String, completion: @escaping (CheckoutViewModel?) -> Void) -> Task {
        let mutation = ClientQuery.mutationForUpdateCheckout(id, updatingEmail: email)
        let task     = self.client.mutateGraphWith(mutation) { response, error in
            error.errorPrint()
            
            if let checkout = response?.checkoutEmailUpdate?.checkout,
                let _ = checkout.email {
                completion(checkout.viewModel)
            } else {
                completion(nil)
            }
        }
        
        task.resume()
        return task
    }
    
    @discardableResult
    func fetchShippingRatesForCheckout(_ id: String, completion: @escaping ((checkout: CheckoutViewModel, rates: [ShippingRateViewModel])?) -> Void) -> Task {
        
        let retry = Graph.RetryHandler<Storefront.QueryRoot>(endurance: .finite(30)) { response, error -> Bool in
            error.errorPrint()
            
            if let response = response {
                return (response.node as! Storefront.Checkout).availableShippingRates?.ready ?? false == false
            } else {
                return false
            }
        }
        
        let query = ClientQuery.queryShippingRatesForCheckout(id)
        let task  = self.client.queryGraphWith(query, retryHandler: retry) { response, error in
            error.errorPrint()
            
            if let response = response,
                let checkout = response.node as? Storefront.Checkout {
                completion((checkout.viewModel, checkout.availableShippingRates!.shippingRates!.viewModels))
            } else {
                completion(nil)
            }
        }
        
        task.resume()
        return task
    }
    
    func completeCheckout(_ checkout: PayCheckout, billingAddress: PayAddress, applePayToken token: String, idempotencyToken: String, completion: @escaping (PaymentViewModel?,Graph.QueryError?) -> Void) {
        
        let mutation = ClientQuery.mutationForCompleteCheckoutUsingApplePay(checkout, billingAddress: billingAddress, token: token, idempotencyToken: idempotencyToken)
        let task     = self.client.mutateGraphWith(mutation) { response, error in
            error.errorPrint()
        
            if let payment = response?.checkoutCompleteWithTokenizedPayment?.payment {
                
                print("Payment created, fetching status...")
                self.fetchCompletedPayment(payment.id.rawValue) { paymentViewModel,error  in
                    completion(paymentViewModel,error)
                }
                
            } else {
                completion(nil,error)
            }
        }
        
        task.resume()
    }
    
    func fetchCompletedPayment(_ id: String, completion: @escaping (PaymentViewModel?,Graph.QueryError?) -> Void) {
        
        let retry = Graph.RetryHandler<Storefront.QueryRoot>(endurance: .finite(30)) { response, error -> Bool in
            error.errorPrint()
            
            if let payment = response?.node as? Storefront.Payment {
                print("Payment not ready yet, retrying...")
                return !payment.ready
            } else {
                return false
            }
        }
        
        let query = ClientQuery.queryForPayment(id)
        let task  = self.client.queryGraphWith(query, retryHandler: retry) { query, error in
            
            if let payment = query?.node as? Storefront.Payment {
                completion(payment.viewModel,error)
            } else {
                completion(nil,error)
            }
        }
        
        task.resume()
    }
    
    
    func fetchCompletedOrder(_ id: String, completion: @escaping ([String:Any]?) -> Void) {
        
        let retry = Graph.RetryHandler<Storefront.QueryRoot>(endurance: .finite(30)) { (response, error) -> Bool in
            return (response?.node as? Storefront.Checkout)?.order == nil
        }
    
        let query = Storefront.buildQuery { $0
            .node(id: GraphQL.ID(rawValue: id)) { $0
                .onCheckout { $0
                    .order { $0
                        .processedAt()
                        .id()
                        .orderNumber()
                        .totalPrice()
                        .currencyCode()
                        
                    }
            
                  
                    
                }
                
            }
        }
        
        let task  = self.client.queryGraphWith(query, retryHandler: retry) { response, error in
            error.errorPrint()
            let checkout = (response?.node as? Storefront.Checkout)
            completion(checkout?.order?.fields)
           print(checkout?.order?.serialize().description)
        }
        task.resume()
    }
    
}




// ----------------------------------
//  MARK: - GraphErrorPrint -
//
extension Optional where Wrapped == Graph.QueryError {
    
    func errorPrint() {
        switch self {
        case .some(let value):
            print("Graph.QueryError: \(value)")
    
        case .none:
            break
        }
    }
     func getMessage(){
        switch self {
        case .some(let value):
            print("Graph.QueryError: \(value)")
            
        case .none:
            break
        }

    }

}


