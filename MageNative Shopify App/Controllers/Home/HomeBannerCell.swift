//
//  HomeBannerCell.swift
//  MageNative Shopify App
//
//  Created by Manohar Singh Rawat on 28/01/20.
//  Copyright © 2020 MageNative. All rights reserved.
//

import UIKit

protocol bannerClicked {
    func bannerDidSelect(banner:banner?,sender:Any)
}

class HomeBannerCell: UITableViewCell {

    @IBOutlet weak var bannerView: FSPagerView!
    
    var banners : [banner]?{
        didSet {
            self.bannerView.automaticSlidingInterval = 2.5
            self.bannerView.dataSource = self
            self.bannerView.delegate = self
            self.bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.bannerView.reloadData()
        }
    }
    var delegate:bannerClicked?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension HomeBannerCell:FSPagerViewDataSource{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners?.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        if let imageUrl = banners![index].url{
            let url = URL(string: imageUrl)
            cell.imageView?.setImageFrom(url)
            cell.imageView?.contentMode = .scaleToFill
            cell.imageView?.clipsToBounds = true
            cell.layer.shadowColor = UIColor.white.cgColor
            cell.imageView?.layer.shadowColor = UIColor.white.cgColor
        }
        
        return cell
    }
}
extension HomeBannerCell:FSPagerViewDelegate{
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        self.delegate?.bannerDidSelect(banner: banners?[index], sender: self)
        
    }
}
