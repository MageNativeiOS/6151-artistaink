//
//  HomeViewController.swift
//  MageNative Shopify App
//
//  Created by Manohar Singh Rawat on 28/01/20.
//  Copyright © 2020 MageNative. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet weak var mainTableView: UITableView!
    var homeData : HomeModel?

    var refreshViewControl: UIRefreshControl?
    
    var cellHeight = [Int:CGFloat]()
    
    fileprivate var homeGridProducts: PageableArray<ProductListViewModel>!
    
    fileprivate var homeGridImage: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        loadHomepageData();
        self.setupTabbarCount()
        let img = UserDefaults.standard.value(forKey: "header") as! String
        if AppSetUp.isDrawerEnabled {
            let _ = self.revealViewController().rearViewController.view
            let tempView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
            
            let imageView = UIImageView()
            imageView.setImageFrom(URL(string: img))
            imageView.frame = CGRect(x: 0, y: 0, width: tempView.frame.width, height: tempView.frame.height)
            imageView.contentMode = .scaleAspectFit
            tempView.addSubview(imageView)
            self.navigationItem.titleView = tempView
            self.navigationItem.titleView?.tintColor = UIColor.black
        }else{
            
            let imageView = UIImageView()
            imageView.setImageFrom(URL(string: img))
            imageView.contentMode = .scaleAspectFit
            self.navigationItem.titleView = imageView
            self.navigationItem.titleView?.tintColor = UIColor.black
        }
        
        // Do any additional setup after loading the view.
    }
    
    func loadData(){
        self.mainTableView.dataSource = self
        self.mainTableView.delegate = self
        refreshViewControl = UIRefreshControl()
        refreshViewControl?.tintColor = UIColor.AppTheme()
        self.mainTableView.addSubview(refreshViewControl!)
        refreshViewControl?.addTarget(self, action: #selector(self.loadHomepageData), for: UIControl.Event.valueChanged)
    }
    
    @objc func loadHomepageData(){
        
        guard let url = (AppSetUp.baseUrl+"shopifymobile/shopifyapi/homepagedata?mid="+Client.merchantID).getURL() else {return}
        var request = URLRequest(url: url)
        request.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
        self.view.addLoader()
        AF.request(request).responseData(completionHandler: {
            response in
            self.refreshViewControl?.endRefreshing()
            self.view.stopLoader()
            switch response.result {
            case .success:
                if let data = response.data {
                    do {
                        self.homeData = try JSONDecoder().decode(HomeModel.self, from: data)
                        self.mainTableView.reloadData()
                        guard let _ = self.homeData?.collections[2]else{return}
                        self.calculateGridCellHeight()
                    }catch let err {
                        print(err.localizedDescription)
                    }
                }
            case .failure:
                guard let error = response.error?.localizedDescription else {return}
                self.showErrorAlert(title: "Error", error:  error)
            }
            
        })
    }

}

extension HomeViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            guard let _ = homeData?.banners.count else{return 0}
            return 1;
        case 1:
            guard let count = homeData?.collections.count else{return 0}
            if(count > 0)
            {
                return 1;
            }
            return 0;
        case 2:
            guard let count = homeData?.collections.count else{return 0}
            if(count > 1)
            {
                return 1;
            }
            return 0;
        case 3:
            guard let _ = homeData?.bannersAdditional?.middle else{return 0}
            return 1;
        
        case 4:
            guard let count = homeGridProducts else{return 0}
            if(count.items.count > 0)
            {
                return 1;
            }
            return 0;
        case 5:
            guard let count = homeData?.collections.count else{return 0}
            if(count > 3)
            {
                return 1;
            }
            return 0;
        case 6:
            guard let _ = homeData?.bannersAdditional?.bottom else{return 0}
            return 1;
        case 7:
            guard let count = homeData?.collections.count else{return 0}
            if(count > 4)
            {
                return 1;
            }
            return 0;
        case 8:
            guard let count = homeData?.collections.count else{return 0}
            if(count > 5)
            {
                return 1;
            }
            return 0;
        case 9:
            guard let _ = homeData?.email else{return 0}
            return recentlyViewedManager.shared.recentlyViewedProduct.count > 0 ? 1 : 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerCell.className) as! HomeBannerCell
            cell.delegate = self;
            cell.banners = self.homeData?.banners
            return cell;
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeSliderCell.className) as! HomeSliderCell
            cell.delegate = self
            cell.parentView = self
            cell.configure(from: self.homeData?.collections[0])
            cell.viewAll.tag=0
            cell.viewAll.addTarget(self, action: #selector(viewAllPressed(_:)), for: .touchUpInside)
            return cell;
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCollectionCell.className) as! HomeCollectionCell
            cell.delegate = self
            cell.parentView = self
            cell.configure(from: self.homeData?.collections[1])
            cell.viewAll.tag=1
            cell.viewAll.addTarget(self, action: #selector(viewAllPressed(_:)), for: .touchUpInside)
            return cell;
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerCell.className) as! HomeBannerCell
            cell.delegate = self;
            cell.banners = self.homeData?.bannersAdditional?.middle
            return cell;
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeGridCell.className) as! HomeGridCell
            cell.parentView = self;
            cell.delegate = self;
            cell.gridImageView.setImageFrom(homeGridImage)
            cell.topLabel.text = self.homeData?.collections[2].title
            //cell.topLabel.text = String(htmlEncodedString: self.homeData?.collections[2].title)
            cell.configure(from: self.homeGridProducts)
            cell.viewAll.tag=2
            cell.viewAll.addTarget(self, action: #selector(viewAllPressed(_:)), for: .touchUpInside)
            return cell;
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeSliderCell1") as! HomeSliderCell
            cell.delegate = self
            cell.parentView = self
            cell.viewAll.tag=3
            cell.viewAll.addTarget(self, action: #selector(viewAllPressed(_:)), for: .touchUpInside)
            if let collection = self.homeData?.collections[3]{
                cell.configure(from: collection)
            }
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerCell.className) as! HomeBannerCell
            cell.delegate = self;
            cell.banners = self.homeData?.bannersAdditional?.bottom
            return cell;
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionCell1") as! HomeCollectionCell
            cell.delegate = self
            if let collection = self.homeData?.collections[4]{
                cell.configure(from: collection)
            }
            cell.parentView = self
            cell.viewAll.tag=4
            cell.viewAll.addTarget(self, action: #selector(viewAllPressed(_:)), for: .touchUpInside)
            return cell;
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeSliderCell2") as! HomeSliderCell
            cell.delegate = self
            cell.parentView = self
            cell.viewAll.tag=5
            cell.viewAll.addTarget(self, action: #selector(viewAllPressed(_:)), for: .touchUpInside)
            if let collection = self.homeData?.collections[5]{
                cell.configure(from: collection)
            }
            return cell
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeRecentlyViewedCell.className) as! HomeRecentlyViewedCell
            cell.delegate = self
            cell.topLabel.text = "Recently Viewed"
            cell.parentView = self
            cell.products = recentlyViewedManager.shared.recentlyViewedProduct
            return cell;
        default:
            return UITableViewCell()
        }
    }
    @objc func viewAllPressed(_ sender:UIButton){
        let getCollection = self.homeData?.collections[sender.tag]
        let coll = collection(id: getCollection?.id, title: getCollection?.title)
        let viewControl:ProductListViewController = self.storyboard!.instantiateViewController()
        viewControl.isfromHome = true
        viewControl.collect = coll
        viewControl.title = coll.title
        //viewControl.title = String(htmlEncodedString: coll.title!)
        self.navigationController?.pushViewController(viewControl, animated: true)
    }
}

extension HomeViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0,3,6:
            return self.view.frame.width*(3/7)
        case 1,5,8,9:
            return 300
        case 2,7:
            return 101*3 + 55
        case 4:
            return cellHeight[4] ?? 0
        default:
            return 0
        }
    }
}

extension HomeViewController:productClicked{
    func productCellClicked(product: ProductViewModel, sender: Any) {
        let productViewController:ProductViewController = self.storyboard!.instantiateViewController()
        productViewController.product = product
        self.navigationController?.pushViewController(productViewController, animated: true)
    }
}


extension HomeViewController:bannerClicked{
    func bannerDidSelect(banner: banner?, sender: Any) {
        guard let banner = banner else {return}
        switch banner.link_to {
        case "product":
            let viewController:ProductViewController = storyboard!.instantiateViewController()
            let str="gid://shopify/Product/"+banner.id!
            let str1 = (str).data(using: String.Encoding.utf8)
            let base64 = str1!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            viewController.productId = base64
            viewController.isProductLoading = true
            self.navigationController?.pushViewController(viewController, animated: true)
        case "collection":
            let coll = collection(id: banner.id, title: banner.link_to)
            print(coll)
            let viewControl:ProductListViewController = self.storyboard!.instantiateViewController()
            viewControl.isfromHome = true
            viewControl.collect = coll
            self.navigationController?.pushViewController(viewControl, animated: true)
        default:
            print("webView")
            let viewController:WebViewController = storyboard!.instantiateViewController()
            viewController.url = banner.id?.getURL()
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
extension HomeViewController{
    func calculateGridCellHeight(){
        guard let model = self.homeData?.collections[2]else{return}
        print("---model--\(model.id)\(model.title)")
        Client.shared.fetchProducts(coll: model, limit: 8) { (response, image, error) in
            
            self.homeGridProducts = response;
            self.homeGridImage = image
            var noOfItems = Int()
            var height = self.calculateCellSize(numberOfColumns: 2).height
            if  UIDevice.current.model.lowercased() == "ipad".lowercased() {
                height = self.calculateCellSize(numberOfColumns: 4).height
                if self.homeGridProducts.items.count%4 != 0{
                    noOfItems = self.homeGridProducts.items.count + 1
                }else {
                    noOfItems = self.homeGridProducts.items.count
                }
            }else {
                if self.homeGridProducts.items.count%2 != 0{
                    noOfItems = self.homeGridProducts.items.count + 1
                }else {
                    noOfItems = self.homeGridProducts.items.count
                }
            }
            self.cellHeight[4] = CGFloat(noOfItems/2 ) * height + 90
            self.mainTableView.reloadSections(NSIndexSet(index: 4) as IndexSet, with: .none)
            //self.productsCollectionView.reloadData()
        }
    }
    
    func calculateCellSize (numberOfColumns columns:Int, of height:CGFloat = 80.0) -> CGSize {
        
        let itemSpacing    = 5 * CGFloat(columns - 1)
        let sectionSpacing: CGFloat = 10
        let length         = (UIScreen.main.bounds.width - itemSpacing - sectionSpacing) / CGFloat(columns)
        return CGSize(
            width:  length,
            height: length + height
        )
    }
}
