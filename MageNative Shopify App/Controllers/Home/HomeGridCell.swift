//
//  HomeGridCell.swift
//  MageNative Shopify App
//
//  Created by Manohar Singh Rawat on 28/01/20.
//  Copyright © 2020 MageNative. All rights reserved.
//

import UIKit

class HomeGridCell: UITableViewCell {

    @IBOutlet weak var gridImageView: UIImageView!
    
    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    @IBOutlet weak var topLabel: UILabel!
    
    @IBOutlet weak var viewAll: UIButton!
    
    var parentView = HomeViewController()
    
    var delegate: productClicked?
    
    fileprivate var products: PageableArray<ProductListViewModel>!{
        didSet{
            self.productsCollectionView.delegate = self;
            self.productsCollectionView.dataSource = self;
            self.productsCollectionView.reloadData();
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(from model: PageableArray<ProductListViewModel>){
        self.products = model;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension HomeGridCell: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.className, for: indexPath) as! ProductCollectionViewCell
        cell.setupView((self.products?.items[indexPath.row].model?.node.viewModel)!)
        cell.delegate = self
        return cell;
    }
    
}

extension HomeGridCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if  UIDevice.current.model.lowercased() == "ipad".lowercased() {
            return collectionView.calculateCellSize(numberOfColumns: 4)
        }else {
            return collectionView.calculateCellSize(numberOfColumns: 2)
        }
    }
}
extension HomeGridCell:wishListDelegate{
    func addToWishListProduct(_ cell: ProductCollectionViewCell, didAddToWishList sender: Any) {
        guard let indexPath = self.productsCollectionView.indexPath(for: cell) else {return}
        let product = self.products.items[indexPath.row]
        guard let productModel = product.model?.node.viewModel else {return}
        let wishProduct = CartProduct.init(product:productModel , variant: product.variants.items.first!)
        if WishlistManager.shared.isProductinWishlist(product: productModel) {
            WishlistManager.shared.removeFromWishList(wishProduct)
        }else {
            WishlistManager.shared.addToWishList(wishProduct)
        }
        self.productsCollectionView.reloadItems(at: [indexPath])
        parentView.setupTabbarCount()
    }
}


extension HomeGridCell:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.productCellClicked(product: (products.items[indexPath.row].model?.node.viewModel)!, sender: self)
    }
}
