//
//  HomeSingleBannerCell.swift
//  MageNative Shopify App
//
//  Created by Manohar Singh Rawat on 28/01/20.
//  Copyright © 2020 MageNative. All rights reserved.
//

import UIKit

class HomeSingleBannerCell: UITableViewCell {

    @IBOutlet weak var bannerImageView: UIImageView!
    
    var banners: banner?{
        didSet{
            bannerImageView.setImageFrom(banners?.url?.getURL())
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
