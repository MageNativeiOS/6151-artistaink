/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppSetUp.isDrawerEnabled {
            self.setupNavBarButton()
        }
        if let revealController = self.revealViewController() {
            revealController.panGestureRecognizer()
            revealController.tapGestureRecognizer()
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.black ]
         //self.navigationController?.navigationBar.tintColor = .black
        if #available(iOS 13.0, *) {
            self.navigationController?.navigationBar.tintColor = UIColor.secondaryLabel
        }
    }
    
    func setupNavBarButton(){
        let barButtonItem = UIBarButtonItem()
        barButtonItem.image = #imageLiteral(resourceName: "hamMenu")
        barButtonItem.target = self.revealViewController()
        barButtonItem.action = #selector(SWRevealViewController.revealToggle(_:))
    self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationItem.leftBarButtonItem = barButtonItem
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.secondaryLabel
        }
        
//        let notificatbarButtonItem = UIBarButtonItem()
//        notificatbarButtonItem.image = #imageLiteral(resourceName: "notification")
//        notificatbarButtonItem.target = self.revealViewController()
//      
//        self.navigationItem.rightBarButtonItem = notificatbarButtonItem
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
