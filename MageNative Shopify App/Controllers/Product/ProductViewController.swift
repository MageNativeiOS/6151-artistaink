/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import PassKit


class ProductViewController: UIViewController {
    var product : ProductViewModel!
    var relatedProducts : PageableArray<ProductListViewModel>?
    @IBOutlet weak var tableView: UITableView!
    var descriptionHeight :CGFloat! = 0.0
    var descriptionLoading = false
    var showDescription = true
    fileprivate var paySession: PaySession?
    var selectedVariant:VariantViewModel!
    var qty = "1";
    var productId:String?
    var isProductLoading = false
    var screenwidth = UIScreen.main.bounds.width
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.tabBarController?.tabBar.isHidden = false

        if isProductLoading{
            self.loadProductData()
        }else {
            selectedVariant = self.product.variants.items.first
            recentlyViewedManager.shared.addtoRecentlyViewed(product)
        }
        
    }
    
    
    func loadProductData(){
        guard let productId = productId else {return}
        self.view.addLoader()
        Client.shared.fetchSingleProduct(of: productId){
            response,error   in
            if let response = response {
                self.view.stopLoader()
               self.product = response
                self.selectedVariant = self.product.variants.items.first
                self.tableView.reloadData()
            }else {
                //self.showErrorAlert(error: error?.localizedDescription)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setupTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.register(ProductAddToCartCell.self, forHeaderFooterViewReuseIdentifier: ProductAddToCartCell.className)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
}




extension ProductViewController {
    @objc func showDescription(sender:UIButton){
        if self.showDescription {
            self.showDescription = false
        }else {
            self.showDescription = true
        }
        self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: . fade)
    }
}

extension ProductViewController:SelectVariantCellDelegate{
    func showSelectVariantController(_ cell: ProductSelectVariantCell) {
        
        let productVarientSelectViewController:ProductVariantSelectController = self.storyboard!.instantiateViewController()
        var variant = self.product.variants
        variant.items = []
        for item in self.product.variants.items{
            if item.availableForSale == true{
                variant.items.append(item)
            }
        }
        productVarientSelectViewController.productVariants = variant
        productVarientSelectViewController.referenceOBject = cell
        productVarientSelectViewController.parentViewControl = self
        self.navigationController?.present(productVarientSelectViewController, animated: true, completion: nil)
    }
}


extension ProductViewController:ProductAddToCartDelegate {
    func productAddToCart(_ cell: ProductAddToCartCell, didAddToCart sender: Any) {
        let item = CartProduct(product: product, variant: self.selectedVariant, quantity: Int(qty)!)
        CartManager.shared.addToCart(item)
        print(CartManager.shared.cartProducts.count)
        self.view.showmsg(msg: product.title + " Added to cart".localized)
        self.setupTabbarCount()
    }
    
    
    //Mark: -- ApplePay purchase
    
    func buyWithApplePay(_ cell: ProductAddToCartCell, didAddToCart sender: Any) {
        let item = CartProduct(product: product, variant: product.variants.items[0])
        Client.shared.createCheckout(with: [item], completion: {
            checkout,error  in
            if let checkout = checkout {
                Client.shared.fetchShop(completion: {
                    shop in
                    if let shopName = shop?.shopName{
                        self.authorizePaymentFor(shopName, in: checkout)
                    }
                })
            }
        })
        
        
    }
    
    
    
    func authorizePaymentFor(_ shopName: String, in checkout: CheckoutViewModel) {
        let payCurrency = PayCurrency(currencyCode: "CAD", countryCode: "CA")
        let payItems    = checkout.lineItems.map { item in
            PayLineItem(price: item.individualPrice, quantity: item.quantity)
        }
        
        let payCheckout = PayCheckout(
            id:              checkout.id,
            lineItems:       payItems,
            giftCards:       nil,
            discount:        nil,
            shippingDiscount: nil,
            shippingAddress: nil,
            shippingRate:    nil,
            currencyCode:    checkout.currencyCode,
            subtotalPrice:   checkout.subtotalPrice,
            needsShipping:   checkout.requiresShipping,
            totalTax:        checkout.totalTax,
            paymentDue:      checkout.paymentDue
        )
        
        let paySession      = PaySession(shopName: shopName, checkout: payCheckout, currency: payCurrency, merchantID: Client.merchantID)
        paySession.delegate = self
        self.paySession     = paySession        
        paySession.authorize()
    }
}

extension ProductViewController:shareNsave {
    
    func shareProduct(_cell: ProductShareAndSaveCell, sender: Any) {
        let url = self.product.onlineStoreUrl
        let vc = UIActivityViewController(activityItems: [url as Any], applicationActivities: nil);
        if(UIDevice().model.lowercased() == "ipad".lowercased()){
            vc.popoverPresentationController?.sourceView = sender as? UIView
        }
        self.present(vc, animated: true, completion: nil);
    }
    
    func saveProduct(_cell: ProductShareAndSaveCell, sender: Any) {
        let product = CartProduct(product: self.product, variant: self.product.variants.items.first!)
        guard let sender = sender as? UIButton else {return}
        if  WishlistManager.shared.isProductinWishlist(product: self.product) {
            WishlistManager.shared.removeFromWishList(product)
            sender.setImage(#imageLiteral(resourceName: "wishempty"), for: .normal)
        }else {
            WishlistManager.shared.addToWishList(product)
             sender.setImage(#imageLiteral(resourceName: "wishfilled"), for: .normal)
        }
        self.setupTabbarCount()
    }

}

extension ProductViewController:productClicked{
    func productCellClicked(product: ProductViewModel, sender: Any) {
        let productViewController:ProductViewController = self.storyboard!.instantiateViewController()
        productViewController.product = product
        self.navigationController?.pushViewController(productViewController, animated: true)
    }
}

extension ProductViewController:ProductImageClicked{
    func productImageClicked(indexPath: IndexPath, sender: ProductHeaderViewCell) {
        let productViewController:ProductImageViewController = self.storyboard!.instantiateViewController()
        //productViewController.images = product.images.items
        productViewController.imageurl = product.images.items[indexPath.row].url.absoluteString
        productViewController.modalPresentationStyle = .fullScreen
        self.present(productViewController, animated: false, completion: nil)
    }
    
}


