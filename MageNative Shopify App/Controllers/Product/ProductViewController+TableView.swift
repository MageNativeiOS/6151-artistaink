/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation
import WebKit

private enum productCellType :Int {
    case header
    case shareNsave
    case title
    case SelectVariant
    case qty
    case description
}
extension ProductViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch productCellType(rawValue: indexPath.row)! {
        case .header :
            return   CGFloat(ClientQuery.maxImageDimension)
        case .title:
            return UITableView.automaticDimension
        case .shareNsave :
            return UITableView.automaticDimension
        case  .description :
            return self.showDescription ? descriptionHeight : 50
        case .qty :
            return 100
        case .SelectVariant:
            if selectedVariant.selectedOptions.first?.name == "Title" && selectedVariant.selectedOptions.first?.value == "Default Title"{
                return 0
            }else{
                return UITableView.automaticDimension
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 50
        default:
            return 0
        }
        
    }
}


extension ProductViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.product != nil ? 6 : 0
        default:
            guard let _  = self.relatedProducts?.items.count else {return 0}
            return 1
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            
            switch productCellType(rawValue: indexPath.row)! {
            case .header :
                let cell       = tableView.dequeueReusableCell(withIdentifier: ProductHeaderViewCell.className, for: indexPath) as! ProductHeaderViewCell
                cell.delegate = self
                cell.images = self.product.images.items
                return cell
            case .title :
                let cell       = tableView.dequeueReusableCell(withIdentifier: ProductTitleViewCell.className, for: indexPath) as! ProductTitleViewCell
                //let fmt = NumberFormatter()
                //fmt.maximumFractionDigits = 3
                //fmt.minimumFractionDigits = 3
                let currencyValue = UserDefaults.standard.value(forKey: "currency") as! String
                //let outputPrice = fmt.string(from: selectedVariant.price as NSNumber)!
                cell.textLabel?.text = Currency.stringFrom(selectedVariant.price, currency: currencyValue)
                cell.detailTextLabel?.text = product.title
                
                return cell
                
            case .shareNsave :
                let cell       = tableView.dequeueReusableCell(withIdentifier: ProductShareAndSaveCell.className, for: indexPath) as! ProductShareAndSaveCell
                cell.setUPWishImage(model: self.product)
                cell.delegate = self 
                return cell
            case .description :
                let cell       = tableView.dequeueReusableCell(withIdentifier: ProductDescriptionCell.className, for: indexPath) as! ProductDescriptionCell
            
                DispatchQueue.main.async {
                    let str="""
                    <html><head><meta name='viewport' content='width=device-width,
                    initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'><link rel="stylesheet" type="text/css" href="webfile.css"><source media="(prefers-color-scheme: dark)"><source media="(prefers-color-scheme: dark)"><style>body{width:\(self.screenwidth-30); !important} p{width:\(self.screenwidth-30); !important} div{width:\(self.screenwidth-30) !important} iframe{width:\(self.screenwidth-30) !important} img{width:\(self.screenwidth-30) !important}</style></head><body>\(self.product.summary)</body></html>
                    """
                    
                    cell.descriptHtml.loadHTMLString(str, baseURL: URL(fileURLWithPath: Bundle.main.path(forResource: "webfile", ofType: "css") ?? "" ))
                    cell.descriptHtml.navigationDelegate = self
                    
                }
                return cell
            case .qty :
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductSelectQuantityCell", for: indexPath) as! ProductSelectQuantityCell
                cell.parent = self;
                return cell;
            case .SelectVariant :
                let cell       = tableView.dequeueReusableCell(withIdentifier: ProductSelectVariantCell.className, for: indexPath) as! ProductSelectVariantCell
                cell.delegate = self
                cell.configure(from: selectedVariant,for: self)
                return cell
            }
        default:
            let cell       = tableView.dequeueReusableCell(withIdentifier: ProductRelatedProductsCell.className, for: indexPath) as! ProductRelatedProductsCell
            cell.products = self.relatedProducts
            cell.delegate = self
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 0:
            if product != nil {
                let cell       = ProductAddToCartCell(reuseIdentifier: ProductAddToCartCell.className)
                cell.delegate = self
                cell.setupAddtocart(availableForSale: product.model!.availableForSale)
                return cell
            }else {
                return nil
            }
        
        default:
            return nil
        }
        
    }
    
    
}
