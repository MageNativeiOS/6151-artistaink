//
//  ProductViewController+WKWebView.swift
//  MageNative Shopify App
//
//  Created by Manohar Singh Rawat on 26/01/20.
//  Copyright © 2020 MageNative. All rights reserved.
//

import Foundation
import WebKit

extension ProductViewController: WKNavigationDelegate, WKUIDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        if !self.descriptionLoading {
            
           // webView.frame.size = webView.sizeThatFits(.zero)
            webView.evaluateJavaScript("document.body.offsetHeight", completionHandler: { (height, error) in
                guard let height = height as? CGFloat else{return;}
                webView.frame.size.height = 1
                self.descriptionHeight = height + 100.0
                
                self.tableView.reloadData()
                self.descriptionLoading = true
                
                
            })
        }
        
        
    }
}
