/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import SafariServices
import UserNotifications
class CartViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let cartManager = CartManager.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.checkForCartUpdate()
        self.tabBarController?.tabBar.isHidden=false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // self.getAccessLocalNotification()
    }
    
    
    func checkForCartUpdate(){
        if CartManager.shared.cartProducts.count > 0 {
            let cartItems = cartManager.cartProducts
            self.view.addLoader()
            Client.shared.createCheckout(with: cartItems) { checkout,error   in
                self.view.stopLoader()
                if let _ = checkout {
                    self.setupTabbarCount()
                }else {
                    //  self.showErrorAlert(errors: error)
                    self.cartManager.showCartQuantityError(on: self, error: error)
                    error?.forEach({
                        let errormsg = $0.errorMessage
                        guard let errors =  $0.errorFields else {return}
                        if errors.contains("variantId") && errors.count == 4 {
                            guard let index = Int(errors[2]) else {return}
                            self.cartManager.updateCartQuantity(at: index, with: 0)
                            
                        }
                        if errors.contains("quantity") && errors.count == 4 {
                            guard let index = Int(errors[2]) else {return}
                            let quantity = errormsg.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                            guard let intVal = Int(quantity) else {return}
                            self.cartManager.updateCartQuantity(at: index, with: intVal)
                            
                        }
                    })
                    self.setupTabbarCount()
                    self.tableView.reloadData()
                }
                
            }
            self.tableView.reloadData()
        }else {
             self.tableView.reloadData()
        }
    }
    
    func setupTable(){
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.tableView.register(CartCheckoutCell.self, forHeaderFooterViewReuseIdentifier: CartCheckoutCell.className)
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
   
    
}

extension CartViewController:UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cartManager.cartProducts.count > 0 ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  cartManager.cartProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: CartProductCell.className) as! CartProductCell
            cell.delegate = self
            cell.configure(from: cartManager.cartProducts[indexPath.row].viewModel)
            return cell
    }
    
    
}

extension CartViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < cartManager.cartProducts.count  {
        let product = cartManager.cartProducts[indexPath.row]
        let productViewController:ProductViewController = self.storyboard!.instantiateViewController()
        productViewController.product = product.productModel
        productViewController.selectedVariant = product.variant
        self.navigationController?.pushViewController(productViewController, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let cell       = CartCheckoutCell(reuseIdentifier: CartCheckoutCell.className)
        cell.cardView()
        cell.delegate = self
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row < cartManager.cartProducts.count {
            return true
        }else {
            return false
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            cartManager.deleteQty(at: indexPath.row)
            tableView.reloadData()
            self.setupTabbarCount()
            
        default:
            break
        }
    }
}

extension CartViewController:proceedToCheckoutDelegate{
    func proceedToCheckout(_ cell: CartCheckoutCell, didAddToCart sender: Any, ofType checkoutType: checkoutType) {
        let cartItems = cartManager.cartProducts
        self.view.addLoader()
        Client.shared.createCheckout(with: cartItems) { checkout,error   in
            self.view.stopLoader()
           
            guard let checkout = checkout else {
                //self.showErrorAlert(errors: error)
                return
            }
            
            
            let checkoutCreate: (CheckoutViewModel) -> Void = { checkout in
                switch checkoutType {
                case .webcheckout:
                    self.openWebCheckout(checkout)
                    
                case .applePay:
                    Client.shared.fetchShop { shop in
                        guard let shopName = shop?.shopName else {
                            print("Failed to fetch shop name.")
                            return
                        }
                        
                        //  self.authorizePaymentFor(shopName, in: checkout)
                    }
                }
            }
            
            self.promptForDiscountCode { discountCode in
                if let discountCode = discountCode {
                    
                    Client.shared.applyCouponCode(discountCode, to: checkout.id) { checkout,error  in
                        if let checkout = checkout {
                            checkoutCreate(checkout)
                        } else {
                             //self.showErrorAlert(title: "Error".localized, error: error?.localizedDescription)
                        }
                    }
                    
                } else {
                    checkoutCreate(checkout)
                }
            }
            
        }
    }
    
    
    // ----------------------------------
    //  MARK: - Discount Codes -
    //
    func promptForDiscountCode(completion: @escaping (String?) -> Void) {
        let alert = UIAlertController(title: "Want to Apply Discount Code?".localized, message: "You can apply discount code here.".localized, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.attributedPlaceholder = NSAttributedString(string: "Discount code".localized)
        }
        
        alert.addAction(UIAlertAction(title: "No".localized, style: .default, handler: { action in
            completion(nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Yes, apply code".localized, style: .cancel, handler: { [unowned alert] action in
            let code = alert.textFields!.first!.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            if let code = code, code.count > 0 {
                completion(code)
            } else {
                completion(nil)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension CartViewController:cartQuantityUpdate {
    func updateCartQuantity(sender: CartProductCell, quantity: Int, model: CartViewModel?) {
        guard let index = tableView.indexPath(for: sender) else {return}
        if quantity <= 0{
            cartManager.deleteQty(at: index.row)
            tableView.reloadData()
            self.setupTabbarCount()
        }else{
            cartManager.updateCartQuantity(at: index.row, with: quantity)
            self.tableView.beginUpdates()
            self.tableView.reloadRows(at: [index], with: .fade)
            if let cell = self.tableView.footerView(forSection: index.section) as? CartCheckoutCell {
                cell.setupTotal()
            }
            self.tableView.endUpdates()
            self.checkForCartUpdate()
        }
        
    }
    
   
}


extension CartViewController{
    func openWebCheckout(_ checkout: CheckoutViewModel){
        //self.cartManager.deleteAll()
        //self.setupTabbarCount()
        let vc:WebViewController = storyboard!.instantiateViewController()
        vc.isLoginRequired=true
        vc.url=checkout.webURL
        vc.ischeckout=true
        self.navigationController?.pushViewController(vc, animated: true)
        
        Client.shared.fetchCompletedOrder(checkout.id, completion: {
            response  in
            
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["magenative_cart_notification_center"])
        })
    }
    

}

extension CartViewController:DZNEmptyDataSetSource,DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: EmptyData.cartEmptyTitle)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return self.cartManager.cartCount == 0
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state:UIControl.State) -> NSAttributedString! {
        return NSAttributedString(string: "Start Shopping".localized)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: EmptyData.cartDescription)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.tabBarController?.selectedIndex = 0
    }
   
}



