/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import UIKit

class HomeNavigationController: UINavigationController {
    var product : ProductViewModel!
    var selectedVariant:VariantViewModel!
    
    var productId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey: "MageNotificationData") != nil
        {
            let data=UserDefaults.standard.value(forKey: "MageNotificationData") as! [String:String]
            UserDefaults.standard.removeObject(forKey: "MageNotificationData")
            
            let notificationData = UserDefaults.standard.value(forKey: "NotificationData") as! [String:String]
            productId = notificationData["product"]
            print(productId)
            
            if (data["link_type"] == "product"){
                
                Client.shared.fetchSingleProduct(of: productId!){
                    response,error   in
                    if let response = response {
                        self.view.stopLoader()
                        self.product = response
                        self.selectedVariant = self.product.variants.items.first
                        print(self.product.model?.id)
                        let productview:ProductViewController = self.storyboard!.instantiateViewController()
                        productview.product = self.product
                        productview.selectedVariant = self.selectedVariant
                        self.pushViewController(productview
                            , animated: true)
                    }else {
                        //self.showErrorAlert(error: error?.localizedDescription)
                    }
                }
                
            }else  if (data["link_type"] == "collection"){
                let coll = collection(id: data["link_id"], title: "collection")
                print(coll)
                let viewControl:ProductListViewController = self.storyboard!.instantiateViewController()
                viewControl.isfromHome = true
                viewControl.collect = coll
                self.pushViewController(viewControl, animated: true)
                
            }else  if (data["link_type"] == "web_address"){
                let webView:WebViewController = self.storyboard!.instantiateViewController()
                print(data["link_id"]?.getURL())
                webView.url = data["link_id"]?.getURL()
                self.pushViewController(webView
                    , animated: true)
                
            }else if (data["link_type"] == "cart"){
                let cartView:CartViewController = self.storyboard!.instantiateViewController()
                self.pushViewController(cartView
                    , animated: true)
                
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
