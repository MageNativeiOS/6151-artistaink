/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class ProductListViewController: UIViewController {
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var collection : CollectionViewModel!
    fileprivate var products: PageableArray<ProductListViewModel>!
    fileprivate let columns:  Int = 2
    var isFromSearch=false
    var collect:collection?
    var isfromHome = false
    var fetchAllProduct = false
    var searchText:String?
    var refreshViewControl: UIRefreshControl?
    var productSortKey:Storefront.ProductCollectionSortKeys? = nil
    var productreverseKey:Bool? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
        sortButton.layer.cornerRadius = 25
        sortButton.backgroundColor = UIColor.AppTheme()
        sortButton.addTarget(self, action: #selector(self.showSortOptions(sender:)), for: .touchUpInside)
        self.view.bringSubviewToFront(sortButton)
        /*if isFromSearch{
            self.view.addLoader()
            guard let searchText = searchText else {return}
            
            Client.shared.searchProductsForQuery(for: searchText, completion: {
                response,error   in
                self.view.stopLoader()
                if let response = response {
                    self.products = response
                    self.products.items=[]
                    for item in response.items{
                        if item.model?.node.availableForSale == true{
                            self.products.items.append(item)
                        }
                    }
                }else {
                    //self.showErrorAlert(error: error?.localizedDescription)
                }
                self.collectionView.reloadData()
            })
        }else {
            loadProducts()
        }*/
        loadProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if products?.items.count != 0 {
            self.collectionView.reloadData()
        }
    }
    
    func loadProducts(sortKey:Storefront.ProductCollectionSortKeys? = nil,cursor: String? = nil,reverse:Bool? = nil){
        self.view.addLoader()
        if isfromHome{
            if fetchAllProduct {
                Client.shared.fetchShopAllProducts(after:cursor,completion: {
                    response in
                    self.view.stopLoader()
                    if let product = response{
                        print("---cursor--\(cursor ?? "")")
                        if cursor != nil {
                            self.products.appendPage(from: product)
                        }else {
                            self.products = product
                            self.products.items=[]
                            for item in product.items{
                                if item.model?.node.availableForSale == true{
                                    self.products.items.append(item)
                                }
                            }
                        }
                        self.collectionView.reloadData()
                    }
                })
            }else {
                
                Client.shared.fetchProducts(coll: collect,sortKey:sortKey,reverse: reverse,after:cursor, completion: {
                    products,url,error  in
                    self.view.stopLoader()
                    self.collectionView.reloadEmptyDataSet()
                    if let products = products{
                        if cursor != nil {
                            self.products.appendPage(from: products)
                        }else {
                            self.products = products
                            self.products.items=[]
                            for item in products.items{
                                if item.model?.node.availableForSale == true{
                                    self.products.items.append(item)
                                }
                            }
                        }
                        self.collectionView.reloadData()
                    }else {
                        //self.showErrorAlert(error: error?.localizedDescription)
                    }
                })
            }
        }else if  isFromSearch{
            guard let searchText = searchText else {return}
            Client.shared.searchProductsForQuery(for: searchText,after:cursor, completion: {
                response,error   in
                self.view.stopLoader()
                if let response = response {
                    print("--cursors--\(cursor ?? "")")
                    if cursor != nil {
                        print(response)
                        self.products.appendPage(from: response)
                    }else {
                        self.products = response
                        self.products.items=[]
                        for item in response.items{
                            if item.model?.node.availableForSale == true{
                                self.products.items.append(item)
                            }
                        }
                    }
                    
                    
                    /*self.products = response
                    self.products.items=[]
                    for item in response.items{
                        if item.model?.node.availableForSale == true{
                            self.products.items.append(item)
                        }
                    }*/
                    
                    self.collectionView.reloadData()
                    
                }else {
                    //self.showErrorAlert(error: error?.localizedDescription)
                }
            })}else  {
            Client.shared.fetchProducts(in: collection,sortKey:sortKey,reverse: reverse,after:cursor, completion: {
                products,_,error  in
                self.view.stopLoader()
                
                if let products = products{
                    print("---cursor--\(cursor ?? "")")
                    if cursor != nil {
                        print("title--"+products.items[0].title)
                        self.products.appendPage(from: products)
                    }else {
                        self.products = products
                        self.products.items=[]
                        for item in products.items{
                            if item.model?.node.availableForSale == true{
                                self.products.items.append(item)
                            }
                        }
                    }
                    self.collectionView.reloadData()
                }else {
                    //self.showErrorAlert(error: error?.localizedDescription)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.emptyDataSetSource = self
        self.collectionView.emptyDataSetDelegate = self
        
    }
    
    
    @objc func showSortOptions(sender:UIButton){
        let alertController=UIAlertController(title: "", message: "Select Option.".localized, preferredStyle: .alert)
        let sortKeys = ["Popularity".localized,"Price: High to Low".localized,"Price: Low to High".localized,"Name: A to Z".localized,"Name: Z to A".localized]
        for item in sortKeys{
            let action = UIAlertAction(title: item, style: .default, handler: {  Void in
                if item == "Popularity".localized{
                    self.productSortKey = Storefront.ProductCollectionSortKeys.bestSelling
                    self.productreverseKey = false
                    self.loadProducts(sortKey: Storefront.ProductCollectionSortKeys.bestSelling,reverse:false)
                }else if item == "Price: High to Low".localized{
                    self.productSortKey = Storefront.ProductCollectionSortKeys.price
                    self.productreverseKey = true
                    self.loadProducts(sortKey: Storefront.ProductCollectionSortKeys.price,reverse:true)
                }else if item == "Price: Low to High".localized{
                    self.productSortKey = Storefront.ProductCollectionSortKeys.price
                    self.productreverseKey = false
                    self.loadProducts(sortKey: Storefront.ProductCollectionSortKeys.price,reverse:false)
                }else if item == "Name: A to Z".localized{
                    self.productSortKey = Storefront.ProductCollectionSortKeys.title
                    self.productreverseKey = false
                    self.loadProducts(sortKey: Storefront.ProductCollectionSortKeys.title,reverse:false)
                }else if item == "Name: Z to A".localized{
                    self.productSortKey = Storefront.ProductCollectionSortKeys.title
                    self.productreverseKey = true
                    self.loadProducts(sortKey: Storefront.ProductCollectionSortKeys.title,reverse:true)
                }else{}
            })
            alertController.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title:"Cancel".localized, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        if UIDevice.current.model.lowercased() == "ipad".lowercased(){
            alertController.popoverPresentationController?.sourceView = sender
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset) <= 40 {
            if self.products.hasNextPage {
                 self.loadProducts(sortKey:productSortKey, cursor: self.products.items.last?.cursor,reverse: productreverseKey)
            }
            
        }
    }
    
    func manageTabbar(_ scrollView:UIScrollView){
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
            
            UIView.animate(withDuration: 1, animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                self.tabBarController?.tabBar.layer.zPosition = -1
                
            }, completion: nil)
            
        }else{
            UIView.animate(withDuration: 1, animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                self.tabBarController?.tabBar.layer.zPosition = 0
                
            }, completion: nil)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.layer.zPosition = 0
    }
}

extension ProductListViewController:UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product         = self.products.items[indexPath.row]
        let productViewController:ProductViewController = self.storyboard!.instantiateViewController()
        productViewController.product = product.model?.node.viewModel
        self.navigationController?.pushViewController(productViewController, animated: true)
    }
    
    
    
}


extension ProductListViewController:UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.items.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell    = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.className, for: indexPath) as! ProductCollectionViewCell
        let product = self.products.items[indexPath.item]
        cell.setupView((product.model?.node.viewModel)!)
        cell.delegate = self
        return cell
    }
    
}


extension ProductListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.model.lowercased() == "ipad".lowercased(){
            return collectionView.calculateCellSize(numberOfColumns: 4)
        }
        return collectionView.calculateCellSize(numberOfColumns: self.columns)
    }
}


extension ProductListViewController:wishListDelegate{
    func addToWishListProduct(_ cell: ProductCollectionViewCell, didAddToWishList sender: Any) {
        guard let indexPath = self.collectionView.indexPath(for: cell) else {return}
        let product = self.products.items[indexPath.row]
        guard let productModel = product.model?.node.viewModel else {
            return
        }
        let wishProduct = CartProduct.init(product: productModel, variant: product.variants.items.first!)
        if WishlistManager.shared.isProductinWishlist(product: productModel) {
            WishlistManager.shared.removeFromWishList(wishProduct)
        }else {
            WishlistManager.shared.addToWishList(wishProduct)
        }
        self.setupTabbarCount()
        self.collectionView.reloadItems(at: [indexPath])
    }
}


extension ProductListViewController:DZNEmptyDataSetSource{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: EmptyData.listEmptyTitle)
    }
    
    
}

extension ProductListViewController:DZNEmptyDataSetDelegate{
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        
        return products?.items.count == 0
    }
    
    
    
}



