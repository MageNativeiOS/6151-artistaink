/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {
    
    var webView = WKWebView()
    var url:URL?
    var isLoginRequired = false
    var ischeckout=false
    
    required init?(coder aDecoder: NSCoder) {
        self.webView = WKWebView(frame: CGRect.zero)
        super.init(coder: aDecoder)
        self.webView.navigationDelegate = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        loadPage();
        // Do any additional setup after loading the view.
    }

    
    func loadPage()
    {
        guard let url = url else {return}
        var request = URLRequest(url: url)
        var postString = ""
        if isLoginRequired && Client.shared.isAppLogin(){
            request.url = Client.loginUrl.getURL()
            if let loginInfo = Client.shared.getToken() as? [String:Any] {
                if let email = loginInfo["email"] as? String {
                    print(email)
                    let password = UserDefaults.standard.value(forKey: "password") as! String
                    print(password)
                    postString += "customer[email]=" + email+"&customer[password]="+password
                }
                postString +=  "&form_type="+"customer_login"
                if ischeckout{
                    postString += "&checkout_url=" + url.absoluteString
                }
                else
                {
                    postString += "&order=" + url.lastPathComponent
                    
                }
                
            }
            request.httpBody = postString.data(using: String.Encoding.utf8)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }
        /*let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")*/
        let webconfgi = WKWebViewConfiguration()
        
        let bounds = self.view.bounds
        let toplayoutguide = self.navigationController!.view.frame.origin.y ;
        let frame  = CGRect(x: 0, y: toplayoutguide, width: bounds.width, height: bounds.height-50)
        webView = WKWebView(frame: frame, configuration: webconfgi)
        webView.load(request)
        self.view.addSubview(webView)
        
        webView.navigationDelegate = self
        self.view.addLoader()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        webView.isHidden = true
        webView.isUserInteractionEnabled = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        webView.evaluateJavaScript("var styleTag = document.createElement('style'); styleTag.innerText = \"div.site-header-wrapper, div#shopify-section-footer {display: none;}\"; document.body.appendChild(styleTag);", completionHandler: nil)
        self.view.stopLoader()
        webView.isHidden = false
        webView.isUserInteractionEnabled = true
        let url=webView.url?.absoluteString
        if (url?.contains("thank_you"))!{
            CartManager.shared.deleteAll()
            self.setupTabbarCount()
            let right=UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(continuePressed(_:)))
            self.navigationItem.rightBarButtonItem=right
        }
    }
    
    
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.view.stopLoader()
        let url=webView.url?.absoluteString
        if (url?.contains("thank_you"))!{
            CartManager.shared.deleteAll()
            self.setupTabbarCount()
            let right=UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(continuePressed(_:)))
            self.navigationItem.rightBarButtonItem=right
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            print(url.absoluteString)
            if (url.absoluteString.contains("thank_you")){
                CartManager.shared.deleteAll()
                self.setupTabbarCount()
                let right=UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(continuePressed(_:)))
                self.navigationItem.rightBarButtonItem=right
                self.navigationItem.leftBarButtonItems=[]
                self.navigationItem.setHidesBackButton(true, animated: true)
                
                
                let arrayToBreakToken=url.absoluteString.components(separatedBy: "/checkouts/")
                let tokenArray=arrayToBreakToken[1].components(separatedBy: "/")
                let token = tokenArray[0]
                setOrderRequest(token: token)
                
            }
        }

        decisionHandler(.allow)
    }
    
    func setOrderRequest(token:String){
        guard let url = (AppSetUp.baseUrl+"index.php/shopifymobile/shopifyapi/setorder?mid="+Client.merchantID+"&checkout_token="+token).getURL() else {return}
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        self.view.addLoader()
        AF.request(request).responseData(completionHandler: {
            response in
            self.view.stopLoader()
            switch response.result {
            case .success:
                if let data = response.data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                        print(json)
                    }catch let err {
                        print(err.localizedDescription)
                    }
                }
            case .failure:
                guard let error = response.error?.localizedDescription else {return}
                print(error)
            }
            
        })
    }
    @objc func continuePressed(_ sender: UIButton)
    {
        CartManager.shared.deleteAll()
        self.setupTabbarCount()
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.selectedIndex = 0
        
        
    }
    
}
