/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class AddressesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var addresses : PageableArray<AddressesViewModel>!
    override func viewDidLoad() {
        super.viewDidLoad()
        //loadAddresses()
        setUPTableHeader()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadAddresses()
    }
    
    func loadAddresses(){
        self.view.addLoader()
        Client.shared.fetchCustomerAddresses(completion: {
            response,error  in
            self.view.stopLoader()
            if let response = response {
                self.addresses = response
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.emptyDataSetDelegate = self
                self.tableView.emptyDataSetSource = self
                self.tableView.reloadData()
            }else {
                //self.showErrorAlert(error: error?.localizedDescription)
            }
        })
    }
    
    
    func setUpTable(){
        
    }
    
    func setUPTableHeader(){
        let cell = tableView.dequeueReusableCell(withIdentifier: "addNewAddress")
        if let addAddress = cell?.viewWithTag(785) as? UIButton {
            addAddress.addTarget(self, action: #selector(self.gotoAddAddress), for: .touchUpInside)
        }
        
        self.tableView.tableHeaderView = cell
    }
    
    @objc func gotoAddAddress(){
        let addressView:AddAddressViewController = self.storyboard!.instantiateViewController()
        self.navigationController?.pushViewController(addressView, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AddressesViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddressListViewCell.className)  as! AddressListViewCell
        let address = addresses.items[indexPath.row]
        cell.configureFrom(address)
        cell.trash.addTarget(self, action: #selector(trashClicked(_:)), for: .touchUpInside)
        cell.trash.tag=indexPath.row
        cell.edit.addTarget(self, action: #selector(editClicked(_:)), for: .touchUpInside)
        cell.edit.tag=indexPath.row
        return cell
    }
    @objc func trashClicked(_ sender:UIButton){
        Client.shared.customerDeleteAddress(address: addresses.items[sender.tag], completeion: {
            id,error,netError  in
            if id != nil {
                self.loadAddresses()
            }else if let error = netError {
                self.showErrorAlert(error: error.localizedDescription)
            }
        })
    }
    @objc func editClicked(_ sender:UIButton){
        let addressView:AddAddressViewController = self.storyboard!.instantiateViewController()
        let address = addresses.items[sender.tag]
        addressView.edirAddress = address
        addressView.isEditAddress = true
        self.navigationController?.pushViewController(addressView, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses?.items.count ?? 0
    }
}

extension AddressesViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            Client.shared.customerDeleteAddress(address: addresses.items[indexPath.row], completeion: {
                id,error,netError  in
                if id != nil {
                    self.loadAddresses()
                }else if let error = netError {
                    //self.showErrorAlert(error: error.localizedDescription)
                }
            })
        default:
            break
        }
    }
}

extension  AddressesViewController:DZNEmptyDataSetSource,DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: EmptyData.addressEmptyTitle)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return addresses?.items.count == 0
    }
    
    
}
