/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class OrdersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var orders:PageableArray<OrderViewModel>!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        loadOrders()
    }
    func loadOrders(cursor: String? = nil){
        self.view.addLoader()
        Client.shared.fetchCustomerOrders(after: cursor,completion: {
            response,error  in
            self.view.stopLoader()
            if let response = response {
                if cursor != nil{
                    self.orders.appendPage(from: response)
                }else{
                    self.orders = response
                }
                self.tableView.reloadData()
            }else {
                self.showErrorAlert(error: error?.localizedDescription)
            }
        })
    }
        
        
    func setupTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset) <= 40 {
            if self.orders.hasNextPage {
                self.loadOrders(cursor: self.orders.items.last?.model?.cursor)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

extension OrdersViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyOrdersCell.className) as! MyOrdersCell
        let order = orders.items[indexPath.row]
        cell.configureFrom(order)
        return cell
    }
}


extension OrdersViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let order = orders.items[indexPath.row]
        let orderViewControl:WebViewController = self.storyboard!.instantiateViewController()
        orderViewControl.url = order.customerUrl
        orderViewControl.isLoginRequired = true
        self.navigationController?.pushViewController(orderViewControl, animated: true)
    }
}


extension OrdersViewController:DZNEmptyDataSetSource,DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: EmptyData.orderEmptyTitle)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: EmptyData.orderDescription)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return orders?.items.count == 0
    }
    
    
}
