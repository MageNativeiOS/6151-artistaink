/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class TabbarController: UITabBarController,UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let textColor = UserDefaults.standard.value(forKey: "TextColor")as? String{
            if textColor == "#FFFFFF"{
                self.tabBar.tintColor = UIColor.AppTheme()
            }else{
                self.tabBar.tintColor = UIColor.black
            }
        }else{
            self.tabBar.tintColor = UIColor.black
        }
        
        
        self.delegate = self
        if let items = self.tabBar.items {
            items.forEach({
                $0.imageInsets =  UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0);
                
            })
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let navigation = viewController as? UINavigationController{
            if !Client.shared.isAppLogin() {
                if navigation.restorationIdentifier == "AccountNavigation" {
                    if let loginNavigation = self.storyboard?.instantiateViewController(withIdentifier:"LoginNavigationController") {
                        loginNavigation.modalPresentationStyle = .fullScreen
                        self.present(loginNavigation, animated: true, completion: nil)
                    }
                    return false
                }
            }
        }
        return true
    }
    
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
