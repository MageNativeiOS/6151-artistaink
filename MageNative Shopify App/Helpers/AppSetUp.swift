/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation
final class AppSetUp {
    let shared = AppSetUp()
    static let baseUrl = "https://shopifymobileapp.cedcommerce.com/"
    static let applePayEnable = false
    static let isDrawerEnabled = true
}


struct mageFont{
    static let regularsize :CGFloat = 13.0
    static let headingLabel :CGFloat  = 15.0
    static let smallLabel :CGFloat = 12.0
    static  let boldFont = "GothamBold"
    static let regularFont = "GothamLight"
    static let heavyFont = "GothamBlack"
}

